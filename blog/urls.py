from django.conf.urls import url

from . import views

#urlpatterns = [
#    url(r'^articles/', views.articles, name='articles')
#    url(r'^articles/(?P<num>[0-9]+)/$', views.article, name='article')
#]

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^(?P<article_id>[0-9]+)/$', views.article, name='article'),
]
