from django.contrib import admin

from .models import Document, Keyword, Category

admin.site.site_header = "RSVP America"
admin.site.site_title = "RSVP America"
admin.site.index_title = "Admin"


class DocumentsAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Document Record', {'fields': ('title', 'author', 'publication', 'publication_date', 'file')}),
        ('Document Metadata', {'fields': ('categories', 'keywords')}),
    ]
    filter_horizontal = ('keywords', 'categories')
    list_display = ['title', 'author', 'publication']
    search_fields = ['title']

# Register your models here.
admin.site.register(Document, DocumentsAdmin)
admin.site.register(Keyword)
admin.site.register(Category)

